
var menu = [    {
        isStart : true,
        iconview: "icon-home",
        label: "Dashboard",
        href: "#",
        sub: [],
        haveSub: false
    },
    {
        isStart : false,
        label: "Booking Tours",
        iconview: "icon-agent",
        href: "#",
        sub: [
            {
                isStart : false,
                iconview: "",
                label: "List Booked Tour",
                href: "#",
            },
            {
                isStart : false,
                iconview: "",
                label: "Book a Tour",
                href: "#",
            }

        ],
        haveSub: true
    },

    {
        isStart : false,
        label: "Origin Tours",
        iconview: "icon-doctor",
        href: "#",
        sub: [ {
            isStart : false,
            iconview: "",
            label: "List Tour",
            href: "#",
        },
            {
                isStart : false,
                iconview: "",
                label: "Add Tour",
                href: "#",
            }],
        haveSub: true,

    },
    {
        isStart : false,
        label: "Origin Transfers",
        iconview: "icon-doctor",
        href: "#",
        sub: [ {
            isStart : false,
            iconview: "",
            label: "List Transfers",
            href: "#",
        },
            {
                isStart : false,
                iconview: "",
                label: "Add Transfers",
                href: "#",
            }],
        haveSub: true
    },

    {
        isStart : false,
        label: "Setting",
        iconview: "icon-doctor",
        href: "#",
        sub: [ {
            isStart : false,
            iconview: "",
            label: "Categories",
            href: "#",
        },
            {
                isStart : false,
                iconview: "",
                label: "Destinations",
                href: "#",
            }],
        haveSub: true

    },



];

Vue.component('menu-item', {
    props: ['item'],
    template: '<li class="nav-item" v-bind:class="{start: item.isStart}"> <a :href="item.href" class="nav-link nav-toggle"> <i :class="item.iconview"></i> <span class="title" v-html="item.label"></span> <span v-bind:class="{arrow: item.haveSub,open: item.haveSub }"></span> </a>' +
        '<ul class="sub-menu" v-if="item.haveSub">'+
        '<li class="nav-item" v-for="subItem in item.sub"> <a href="" class="nav-link "> <span class="title" v-html="subItem.label"></span> </a> </li>'+
        '</ul>'+
        ' </li>'

});
var MainMenu = new Vue({
    el: "#lef-menu-admin",
    data: {
        listItems : menu
    }
});

