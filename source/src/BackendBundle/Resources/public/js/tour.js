var DataTable = $.extend(Base,{
    ajaxUrl: $("#ajax-url").val(),
    tableId: '#sample_1',
    status: 1,
    dataSort: {},
    dataPost: {},
    currentSearch: false,
    initPage: function () {
        this.init();
        $('.filter-changing a').on('click', function () {
            $(this).parent().first().find('a').removeClass('active');
            $(this).addClass('active');

            DataTable.status = $(this).attr('data');
            DataTable.currentPage = 1;
            DataTable.getData();
        });
        DataTable.getData();
    },

    getData: function () {
        if( this.ajaxUrl == 'undefined' || this.ajaxUrl == ''){
            return ;
        }
        this.dataPost = {'search': this.currentSearchData, 'length': this.tableLength, 'page': this.currentPage, 'status': this.status, 'sort': this.dataSort};
        $.ajax({
            type: "POST",
            url: DataTable.ajaxUrl,
            data: DataTable.dataPost,
            beforeSend: function () {
                $(DataTable.bodyTag).html('<tr role="row"><td colspan="6" style="text-align: center;"> Loading...</td></tr>');
            },
            success: function (data, textStatus, jqXHR) {
                var result = data['data'];
                var total = data['total'];


                DataTable.generateView(result);
                DataTable.generateInfo(total);
                DataTable.generatePagin(total);
                DataTable.initPaging();
                DataTable.initEventTable();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (typeof errorCallback == 'function')
                    return errorCallback(jqXHR, textStatus, errorThrown);
                return false;
            },
            complete: function (jqXHR, textStatus) {
                DataTable.currentSearch = false;
            }

        });


    },

    initEventTable: function () {
        $("[class='make-switch']").bootstrapSwitch();
        $('.make-switch').on('switchChange.bootstrapSwitch', function () {
            var _this = $(this);
            var _parent = _this.closest('.bootstrap-switch');
            var _labelText = _parent.next('.switch-text');
            $(this).on('ifChecked', function(){
                consle.log('check');

            });


            if (_this.prop('checked', true)) {
                var dataPost = {'id': this.value, 'type': 1};
                var ajaxUrl = $("#update-tour-status-url").val();
                $.ajax({
                    type: "POST",
                    url: ajaxUrl,
                    data: dataPost,

                    success: function (data, textStatus, jqXHR) {

                    }
                });
            }

            if (_parent.hasClass('bootstrap-switch-off')) {
                _labelText.find('.switch-text-on').hide();
                _labelText.find('.switch-text-off').show();
            } else {
                _labelText.find('.switch-text-off').hide();
                _labelText.find('.switch-text-on').show();
            }
        });
    },

    generateView: function (data)
    {
        var result = ''

        result += this.renderRecord(data.data);
      //  result += this.renderHiddenRecord(data);

        if (result == '') {
            result = '<tr role="row"><td colspan="'+$(DataTable.tableId + ' th').length+'">Have no record in result  </td> </tr>';
        }
        $(this.bodyTag).html(result);
    },
    renderRecord(data){

        if($.isEmptyObject(data)) {
            return '';
        }
        var result = '';
        $.each(data,function () {
            var record = '';
            //code
            record += DataTable.generateElement({tag:'td',properties: []},this.code);

            record += DataTable.generateElement({tag:'td',properties: []}, this.name);
            record += DataTable.generateElement({tag:'td',properties: []}, this.registerDate);

            //status

            var inputProperty = [];
            inputProperty.push({type : 'checkbox'});
            inputProperty.push({class : 'make-switch'});
            if(this.status == 1){
                inputProperty.push({checked : true});
            }

            inputProperty.push({"data-on-color" : 'success'});
            inputProperty.push({"data-off-color" : 'danger'});
            inputProperty.push({"data-on-text" : "<i class='fa fa-check'></i>"});
            inputProperty.push({"data-off-text" : "<i class='fa fa-times'></i>"});
            inputProperty.push({"data-size" : 'mini'});
            var viewStatus = DataTable.generateElement({tag:'input',properties: inputProperty});
            var activeClass = '';
            var deactiveClass =  '';
            if(this.status == 1){
                activeClass = 'switch-text-on';
                deactiveClass = 'switch-text-off hide-item';
            } else {
                activeClass = 'switch-text-on hide-item';
                deactiveClass = 'switch-text-off';
            }


            var v1 = DataTable.generateElement({tag:'span',properties: [{class: activeClass}]}, "Activate");
            v1 += DataTable.generateElement({tag:'span',properties: [{class: deactiveClass}]}, "Deactivate");
            v1 = DataTable.generateElement({tag:'div',properties: [{class: "switch-text"}]}, v1);

            viewStatus += v1;
            viewStatus= DataTable.generateElement({tag:'div',properties: [{class: "status-wrapper"}]}, viewStatus);
            record += DataTable.generateElement({tag:'td',properties: []}, viewStatus);

            // action
            var action = '';

            var btnEdit  = DataTable.generateElement({tag:'i',properties: [{class : "fa fa-edit"}]});
            btnEdit = DataTable.generateElement({tag:'a',properties: [{class : "btn btn-ahalf-circle text-uppercase green-seagreen btn-icon-right btn-xs"}, {href: "#"}]}, "Edit " + btnEdit );
            action += btnEdit;
            /*
            var btn1  = DataTable.generateElement({tag:'i',properties: [{class : "fa fa-edit"}]});
            btn1 = DataTable.generateElement({tag:'a',properties: [{class : "btn btn-ahalf-circle text-uppercase green-seagreen btn-icon-right btn-xs"}, {href: "#"}]}, "Sub Agents  " + btn1 );
            action += btn1;

            var btn2  = DataTable.generateElement({tag:'i',properties: [{class : "fa fa-edit"}]});
            btn2 = DataTable.generateElement({tag:'a',properties: [{class : "btn btn-ahalf-circle text-uppercase green-seagreen btn-icon-right btn-xs"}, {href: "#"}]}, "View Doctors  " + btn2 );
            action += btn2;
            */

            var btn3 = DataTable.generateElement({tag:'a',properties: [{class : "btn btn-ahalf-circle text-uppercase red btn-icon-right btn-xs"}, {href: "#"}]}, "Delete" );
            action += btn3;

            record += DataTable.generateElement({tag:'td',properties: []},action);
            result += DataTable.generateElement({tag:'tr',properties: [{class: "row-item rowItem"}, {role : "row"}]},record);

        });

        return result;
    },


});

$(document).ready(function () {
    jQuery.validator.addMethod("empty", function (value, element) {
        return value != 'empty';
    }, "This field is required");
    DataTable.initPage();

});