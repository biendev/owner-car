
var Category = {
    init: function () {
        $("#category_code").select2({placeholder: 'Select Type'});
        this.validateForm();
        $("#create-category-form").find('.button-submit').first().on('click', function (e) {
            e.preventDefault();
            $("#create-category-form").submit();
        });

    },
    validateForm: function () {
        var validobj = $("#create-category-form").validate({
            errorClass: "error",
            errorElement: 'span',
            errorPlacement: function (error, element) {

            },
            rules: {},
            submitHandler: function (form) {
                $(form).find('.button-submit').addClass('disabled');
                form.submit();
            },
            highlight: function (element, errorClass, validClass) {

                var elem = $(element);
                if (elem.hasClass('select2')) {
                    var t = elem.parent().find(".select2-selection").first();
                    t.attr('style', 'border: 2px solid red');

                } else {
                    elem.attr('style', 'border: 2px solid red');
                    elem.addClass(errorClass);
                }
            },
            unhighlight: function (element, errorClass, validClass) {
                var elem = $(element);
                if (elem.hasClass('select2')) {
                    var t = elem.parent().find(".select2-selection").first();
                    t.attr('style', '');
                } else {
                    elem.attr('style', '');
                    elem.removeClass(errorClass);
                }
            }
        });

        $(document).on('change', '.form-control', function () {
            if (!$.isEmptyObject(validobj.submitted)) {
                validobj.form();
            }
        });
    }
};
$(document).ready(function () {
    Category.init();
});