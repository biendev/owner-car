var DataTable = $.extend(Base,{
    ajaxUrl: $("#ajax-url").val(),
    tableId: '#categoryListTable',
    status: 0,
    dataSort: {},
    dataPost: {},
    currentSearch: false,
    initPage: function () {
        this.init();
        $('.filter-changing a').on('click', function () {
            $(this).parent().first().find('a').removeClass('active');
            $(this).addClass('active');

            DataTable.status = $(this).attr('data');
            DataTable.currentPage = 1;
            DataTable.getData();
        });
        DataTable.getData();
    },

    getData: function () {
        if( this.ajaxUrl == 'undefined' || this.ajaxUrl == ''){
            return ;
        }
        this.dataPost = {'search': this.currentSearchData, 'length': this.tableLength, 'page': this.currentPage, 'status': this.status, 'sort': this.dataSort};
        $.ajax({
            type: "POST",
            url: DataTable.ajaxUrl,
            data: DataTable.dataPost,
            beforeSend: function () {
                $(DataTable.bodyTag).html('<tr role="row"><td colspan="6" style="text-align: center;"> Loading...</td></tr>');
            },
            success: function (data, textStatus, jqXHR) {
                var result = data['data'];
                var total = data.data['total'];
                DataTable.generateView(result);
                DataTable.generateInfo(total);
                DataTable.generatePagin(total);
                DataTable.initPaging();
                DataTable.initEventTable();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (typeof errorCallback == 'function')
                    return errorCallback(jqXHR, textStatus, errorThrown);
                return false;
            },
            complete: function (jqXHR, textStatus) {
                DataTable.currentSearch = false;
            }

        });


    },

    initEventTable: function () {
        $("[class='make-switch']").bootstrapSwitch();
        $('.make-switch').on('switchChange.bootstrapSwitch', function () {
            var _this = $(this);
            var _parent = _this.closest('.bootstrap-switch');
            var _labelText = _parent.next('.switch-text');
            $(this).on('ifChecked', function(){
                consle.log('check');

            });


            if (_this.prop('checked', true)) {
                var dataPost = {'id': this.value, 'type': 1};
                var ajaxUrl = $("#update-tour-status-url").val();
                $.ajax({
                    type: "POST",
                    url: ajaxUrl,
                    data: dataPost,

                    success: function (data, textStatus, jqXHR) {

                    }
                });
            }

            if (_parent.hasClass('bootstrap-switch-off')) {
                _labelText.find('.switch-text-on').hide();
                _labelText.find('.switch-text-off').show();
            } else {
                _labelText.find('.switch-text-off').hide();
                _labelText.find('.switch-text-on').show();
            }
        });
    },

    generateView: function (data)
    {
        var result = ''

        result += this.renderRecord(data.data);
      //  result += this.renderHiddenRecord(data);

        if (result == '') {
            result = '<tr role="row"><td colspan="'+$(DataTable.tableId + ' th').length+'">Have no record in result  </td> </tr>';
        }
        $(this.bodyTag).html(result);
    },
    renderRecord(data){

        if($.isEmptyObject(data)) {
            return '';
        }
        var result = '';
        var editUrl = $("#edit-url").val();


        $.each(data,function () {
            var record = '';
            //code
            record += DataTable.generateElement({tag:'td',properties: []}, this.name);
            record += DataTable.generateElement({tag:'td',properties: []}, this.description);
            // action
            var action = '';

            var btnEdit  = DataTable.generateElement({tag:'i',properties: [{class : "fa fa-edit"}]});
            btnEdit = DataTable.generateElement({tag:'a',properties: [{class : "btn btn-ahalf-circle text-uppercase green-seagreen btn-icon-right btn-xs"}, {href: editUrl.replace('id', this.id)}]}, "Edit " + btnEdit );
            action += btnEdit;

            var btn3 = DataTable.generateElement({tag:'a',properties: [{class : "btn btn-ahalf-circle text-uppercase red btn-icon-right btn-xs"}, {href: "#"}]}, "Delete" );
            action += btn3;

            record += DataTable.generateElement({tag:'td',properties: []},action);
            result += DataTable.generateElement({tag:'tr',properties: [{class: "row-item rowItem"}, {role : "row"}]},record);

        });

        return result;
    },


});

$(document).ready(function () {
    jQuery.validator.addMethod("empty", function (value, element) {
        return value != 'empty';
    }, "This field is required");
    DataTable.initPage();

});