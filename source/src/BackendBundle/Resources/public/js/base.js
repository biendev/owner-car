var Base = {
    check: true,
    tableLength: 5,
    currentSearchData: '',
    tableId: '',
    currentPage: 1,
    maxPage : 0 ,
    ajaxUrl: '',
    bodyTag: '#table-body',
    pagingTag: '#table-paging',
    infoTag: "#table-info",
    tableLenthTag: '#table-length',
    tableSearchTag: '#table-search-data',
    tableButtonSearch: "#sample_1_filter .icon-magnifier",
    tableSortAttribute: 'data-colum-sort',
    dataSort: {},
    generatePagin(total){
        var html = '';
        var startPage = parseInt(this.currentPage);
        var max = Math.ceil(total / this.tableLength);
        var text = this.generateElement({tag:'i', properties: [{class:'fa fa-angle-left'}]});
        text = this.generateElement({tag:'a', properties: [{href:'#'}, {title: 'Prev'}]},text);
        if (startPage > 1)
        {
            text = this.generateElement({tag:'li', properties: [{class:'prev'}]},text);
        } else {
            text = this.generateElement({tag:'li', properties: [{class:'prev disabled'}]},text);

        }
        html += text;
        var end =  0;
        if(startPage > 1){
            end = startPage + 3;
        } else {
            end = startPage + 4;
        }

        if (end > max)
        {
            end = max;
        }
        var start = 1;
        if (startPage > 2 && end > 5) {
            start = end - 4;
        }
        for (var i = start; i <= end; i++) {

            var text = this.generateElement({tag:'a', properties: [{href:'#'}]},i);
            if (i == startPage) {
                text = this.generateElement({tag:'li', properties: [{class:'active'}]},text);
            } else {
                text = this.generateElement({tag:'li', properties: []},text);
            }
            html += text;
        }
        var text = this.generateElement({tag:'i', properties: [{class:'fa fa-angle-right'}]});
        text = this.generateElement({tag:'a', properties: [{href:'#'}, {title: 'Next'}]},text);
        if (startPage == max) {
            text = this.generateElement({tag:'li', properties: [{class:'next disabled'}]},text);
        } else {
            text = this.generateElement({tag:'li', properties: [{class:'next'}]},text);
        }
        html += text;
        if (total > 0 && max > 1) {
            $(this.pagingTag).html(html);
        } else {
            $(this.pagingTag).html('');
        }
        this.maxPage = max;


    },
    generateInfo: function (sum) {
        var total = sum | 0;
        var start = (this.currentPage - 1) * this.tableLength + 1;
        var end = this.currentPage * this.tableLength;

        if (end > total) {
            end = total;
        }
        if (total == 0) {
            start = 0;
        }
        $(this.infoTag).html("Showing " + start + " to " + end + " of " + total + " entries")
    },
    generateElement(info,data = ''){

        if(typeof info.tag =='undefined' || info.tag == '' ){
            return ''
        }
        var result =  '<'+info.tag + " " ;

        if(typeof info.properties == 'undefined' || info.properties == '' ){

        } else {
            $.each(info.properties, function(){
                var property = this;
                var key = Object.keys(this);
                if(key.length > 0 ){
                    $.each(key, function () {
                        result += this + '="'+ property[this] + '"';
                    })
                }
            });

        }
        result += ' >';

        result += data;
        result += '</'+ info.tag + '>';

        return result;
    },
    init: function () {

        $(this.tableSearchTag).keyup(function (event) {
//            setTimeout( function(){
//                if(!DataTable.currentSearch){
//                    $("#sample_1_filter .icon-magnifier").click();
//                    DataTable.currentSearch = true;
//                }
//            }, 200 );
            if (event.keyCode == 13) {
                $(Base.tableButtonSearch).click();
            }
        });

        $(this.tableButtonSearch).on('click', function () {
            Base.changeSearchData($(Base.tableSearchTag).val());
        });
        $(this.tableLenthTag).on('change', function () {
            Base.changeTableLenth(this.value);
        });
        this.initPaging();

        $(this.tableId + ' th').on('click', function () {
            var attr = $(this).attr('data-colum-sort');
            if(typeof attr !== typeof undefined && attr !== false) {
                DataTable.changeSort($(this));
                DataTable.getData();
            }
        });
    },
    changeSort: function (e) {
        var sort = e.attr(this.tableSortAttribute);
        $.each($(this.tableId).find('th'), function () {
            if ($(this).attr(this.tableSortAttribute) != sort) {
                $(this).removeClass('sorting_asc');
                $(this).removeClass('sorting_desc');
            }
        });

        this.dataSort = {};

        if (typeof sort != 'undefined') {
            if (e.hasClass('sorting_asc')) {
                e.removeClass('sorting_asc');
                e.addClass('sorting_desc');
                this.dataSort[sort] = 'DESC';
            } else if (e.hasClass('sorting_desc')) {
                e.removeClass('sorting_desc');
                e.addClass('sorting_asc');
                this.dataSort[sort] = 'ASC';
            } else {
                e.addClass('sorting_asc');
                this.dataSort[sort] = 'ASC';
            }
        }

    },
    initPaging: function () {

        $(this.pagingTag + " li").on('click', function (e) {
            e.preventDefault();
            if ($(this).is(':first-child'))
            {
                page = 'des'

            } else if ($(this).is(':last-child')) {

                page = 'inc'
            } else {
                page = $(this).find('a').html();
            }
            DataTable.changePageData(page);

        });
    },

    changeTableLenth: function (length) {
        this.currentPage = 1;
        this.tableLength = length;
        DataTable.getData();
    },

    changePageData: function (page) {
        var valid = true;
        if (page == 'inc') {
            if(this.currentPage == this.maxPage) {
                valid = false;
            } else {
                this.currentPage++;
            }

        } else if (page == 'des') {
            if(this.currentPage == 1) {
                valid = false;
            } else {
                this.currentPage--;
            }

        } else {
            this.currentPage = page;

        }
        if(valid) {
            DataTable.getData();
        }

    },

    changeSearchData: function (dataSearch) {
        this.currentSearchData = dataSearch;
        this.currentPage = 1;
        DataTable.getData();
    },
};