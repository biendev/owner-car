<?php

namespace BackendBundle\Controller;

use BackendBundle\Form\CategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use UtilBundle\Entity\Category;

/**
 * @Route("/setting")
 *
 */
class SettingController extends BaseController
{


    /**
     * @Route("/category/list", name= "category-index")
     */
    public function categoryAction()
    {
        return $this->renderView('@Backend/Category/index.html.twig', ['ajaxUrl' => 'load-list-category-ajax']);
    }


    /**
     * @Route("/category-ajax", name="load-list-category-ajax")
     */
    public function loadListTourAction(Request $request)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $result = $em->getRepository('UtilBundle:Category')->getAdminList($request->request);
        return new JsonResponse(['data' =>$result, 'status' => true]);
    }


    /**
     * @Route("/category/create", name= "category-add")
     */
    public function createCategoryAction(Request $request)
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($category);
            $entityManager->flush();
            return $this->redirect($this->generateUrl('category-index'));
        }
        return $this->renderView('@Backend/Category/add.html.twig', ['ajaxUrl' => 'load-list-category-ajax','form' => $form->createView()]);
    }


    /**
     * @Route("/category/{id}/update", name= "category-update")
     */
    public function updateCategoryAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $category = $em->getRepository('UtilBundle:Category')->find($id);
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($category);
            $entityManager->flush();
            return $this->redirect($this->generateUrl('category-index'));
        }
        return $this->renderView('@Backend/Category/add.html.twig', ['ajaxUrl' => 'load-list-category-ajax','form' => $form->createView()]);
    }



    /**
     * @Route("/update-tuor-status", name="update-tour-status")
     */
    public function updateStatusTourAction()
    {
        $result = [];
        return new JsonResponse(['data' =>$result, 'status' => true]);
    }
}
