<?php

namespace BackendBundle\Controller;

use BackendBundle\Form\TourType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use UtilBundle\Entity\Tours;

/**
 * @Route("/tours")
 *
 */
class TourController extends BaseController
{

    /**
     * @Route("/", name= "tour-index")
     */
    public function indexAction()
    {



        return $this->renderView('@Backend/Tour/index.html.twig', ['ajaxUrl' => 'load-list-ajax']);
    }

    /**
     * @Route("/load-list-ajax", name="load-list-ajax")
     */
    public function loadListTourAction(Request $request)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $result = $em->getRepository('UtilBundle:Tours')->getAdminList($request->request);
        return new JsonResponse(['data' =>$result, 'status' => true]);
    }



    /**
     * @Route("/update-tuor-status", name="update-tour-status")
     */
    public function updateStatusTourAction()
    {
        $result = [];
        return new JsonResponse(['data' =>$result, 'status' => true]);
    }

    /**
     * @Route("/create", name= "tour-add")
     */
    public function createTourAction(Request $request)
    {
        $tour = new Tours();
        $entityManager = $this->getDoctrine()->getManager();
        $form = $this->createForm(TourType::class, $tour, array(
            'entity_manager' => $entityManager,
        ));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tour);
            $entityManager->flush();
            return $this->redirect($this->generateUrl('category-index'));
        }
        return $this->renderView('@Backend/Tour/add.html.twig', ['ajaxUrl' => 'load-list-category-ajax','form' => $form->createView()]);
    }


    /**
     * @Route("/{id}/update", name= "category-update")
     */
    public function updateTourAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $category = $em->getRepository('UtilBundle:Category')->find($id);
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($category);
            $entityManager->flush();
            return $this->redirect($this->generateUrl('category-index'));
        }
        return $this->renderView('@Backend/Category/add.html.twig', ['ajaxUrl' => 'load-list-category-ajax','form' => $form->createView()]);
    }
}
