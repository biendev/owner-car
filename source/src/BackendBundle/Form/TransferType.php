<?php

namespace BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AdminBundle\Form\Type\AdminRadioType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use UtilBundle\Entity\Category;
use UtilBundle\Repository\CategoryRepository;
use UtilBundle\Utility\Constant;


class TransferType extends AbstractType {

    private $em;
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $this->em = $options['entity_manager'];
        $listType = $this->getListType();
        $builder
            ->add('name', TextType::class, array('label' => 'Name', 'attr' => array('placeholder' => 'Enter Name')))
            ->add('description', TextareaType::class, array('label' => 'Description', 'attr' => array('placeholder' => 'Enter Description')))
            ->add('category', EntityType::class, array(
                'class' => Category::class,

                'label' => 'Type',
                'placeholder' => 'Select Type',
                'query_builder' => function (CategoryRepository $er) {
                    return  $er->createQueryBuilder('a')
                        ->where('a.deletedAt is null')
                        ->andWhere( 'a.code =:code')
                        ->setParameter('code', Constant::CATEGORY_CODE_TRANSFER );

                },
                'choice_label' => function ($category) {
                    return $category->getName();
                }
            ))
        ;

    }

    private function getListType(){
        $list = $this->em->getRepository('UtilBundle:Category')->getAdminListType(Constant::CATEGORY_CODE_TOUR);
        $listType = [];
        foreach ($list as $item) {
            $listType[$item['name']] = $item['id'];
        }
        return $listType;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'entity_manager' => null
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('entity_manager');
    }

    /**
     * @return string
     */
    public function getName() {
        return 'tour';
    }

}
