<?php

namespace BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AdminBundle\Form\Type\AdminRadioType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use UtilBundle\Entity\Category;
use UtilBundle\Utility\Constant;


class CategoryType extends AbstractType {


    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
            ->add('name', TextType::class, array('label' => 'Name', 'attr' => array('placeholder' => 'Enter  Name')))
            ->add('description', TextareaType::class, array('label' => 'Description', 'attr' => array('placeholder' => 'Enter Description')))
            ->add('code', ChoiceType::class, array(
                'label' => 'Type',
                'placeholder' => 'Select Type',
                'choices' => Constant::CATEGORY_LIST,
            ))

        ;

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'em' => null,
            'data_class' => Category::class,

        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'tour';
    }

}
