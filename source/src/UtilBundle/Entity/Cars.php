<?php

namespace UtilBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cars
 *
 * @ORM\Table(name="cars")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Cars
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fleet", type="string", length=191, nullable=false)
     */
    private $fleet;

    /**
     * @var string
     *
     * @ORM\Column(name="capability", type="string", length=191, nullable=false)
     */
    private $capability;

    /**
     * @var integer
     *
     * @ORM\Column(name="price", type="integer", nullable=false)
     */
    private $price;

    /**
     * @var integer
     *
     * @ORM\Column(name="baggage", type="integer", nullable=false)
     */
    private $baggage;

    /**
     * @var string
     *
     * @ORM\Column(name="car_image", type="string", length=191, nullable=true)
     */
    private $carImage;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * Gets triggered only on insert

     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update

     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime("now");
    }

    /**
     * Set fleet
     *
     * @param string $fleet
     *
     * @return Cars
     */
    public function setFleet($fleet)
    {
        $this->fleet = $fleet;

        return $this;
    }

    /**
     * Get fleet
     *
     * @return string
     */
    public function getFleet()
    {
        return $this->fleet;
    }

    /**
     * Set capability
     *
     * @param string $capability
     *
     * @return Cars
     */
    public function setCapability($capability)
    {
        $this->capability = $capability;

        return $this;
    }

    /**
     * Get capability
     *
     * @return string
     */
    public function getCapability()
    {
        return $this->capability;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return Cars
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set baggage
     *
     * @param integer $baggage
     *
     * @return Cars
     */
    public function setBaggage($baggage)
    {
        $this->baggage = $baggage;

        return $this;
    }

    /**
     * Get baggage
     *
     * @return integer
     */
    public function getBaggage()
    {
        return $this->baggage;
    }

    /**
     * Set carImage
     *
     * @param string $carImage
     *
     * @return Cars
     */
    public function setCarImage($carImage)
    {
        $this->carImage = $carImage;

        return $this;
    }

    /**
     * Get carImage
     *
     * @return string
     */
    public function getCarImage()
    {
        return $this->carImage;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Cars
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Cars
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Cars
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Cars
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
