<?php

namespace UtilBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TransferNames
 *
 * @ORM\Table(name="transfer_names", indexes={@ORM\Index(name="transfer_names_transfer_id_foreign", columns={"transfer_id"})})
 * @ORM\Entity
 */
class TransferNames
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="pick_up", type="integer", nullable=false)
     */
    private $pickUp;

    /**
     * @var integer
     *
     * @ORM\Column(name="drop_off", type="integer", nullable=false)
     */
    private $dropOff;

    /**
     * @var string
     *
     * @ORM\Column(name="transfer_code", type="string", length=191, nullable=true)
     */
    private $transferCode;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=191, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=191, nullable=false)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="duration", type="string", length=191, nullable=false)
     */
    private $duration;

    /**
     * @var string
     *
     * @ORM\Column(name="thumbnail", type="string", length=191, nullable=true)
     */
    private $thumbnail;

    /**
     * @var string
     *
     * @ORM\Column(name="slide", type="text", length=65535, nullable=true)
     */
    private $slide;

    /**
     * @var string
     *
     * @ORM\Column(name="blog", type="text", nullable=false)
     */
    private $blog;

    /**
     * @var string
     *
     * @ORM\Column(name="discount", type="string", length=191, nullable=true)
     */
    private $discount;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_hot", type="boolean", nullable=false)
     */
    private $isHot = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \Transfer
     *
     * @ORM\ManyToOne(targetEntity="Transfer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="transfer_id", referencedColumnName="id")
     * })
     */
    private $transfer;


}

