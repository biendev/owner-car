<?php

namespace UtilBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SlideImages
 *
 * @ORM\Table(name="slide_images", indexes={@ORM\Index(name="slide_images_source_id_foreign", columns={"source_id"}), @ORM\Index(name="slide_images_slide_id_foreign", columns={"slide_id"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class SlideImages
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \Slides
     *
     * @ORM\ManyToOne(targetEntity="Slides")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="slide_id", referencedColumnName="id")
     * })
     */
    private $slide;

    /**
     * @var \Sources
     *
     * @ORM\ManyToOne(targetEntity="Sources")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="source_id", referencedColumnName="id")
     * })
     */
    private $source;


    /**
     * Gets triggered only on insert

     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update

     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime("now");
    }


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return SlideImages
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return SlideImages
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slide
     *
     * @param \UtilBundle\Entity\Slides $slide
     *
     * @return SlideImages
     */
    public function setSlide(\UtilBundle\Entity\Slides $slide = null)
    {
        $this->slide = $slide;

        return $this;
    }

    /**
     * Get slide
     *
     * @return \UtilBundle\Entity\Slides
     */
    public function getSlide()
    {
        return $this->slide;
    }

    /**
     * Set source
     *
     * @param \UtilBundle\Entity\Sources $source
     *
     * @return SlideImages
     */
    public function setSource(\UtilBundle\Entity\Sources $source = null)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return \UtilBundle\Entity\Sources
     */
    public function getSource()
    {
        return $this->source;
    }
}
