<?php

namespace UtilBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TaggableTaggables
 *
 * @ORM\Table(name="taggable_taggables", indexes={@ORM\Index(name="i_taggable_fwd", columns={"tag_id", "taggable_id"}), @ORM\Index(name="i_taggable_rev", columns={"taggable_id", "tag_id"}), @ORM\Index(name="i_taggable_type", columns={"taggable_type"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class TaggableTaggables
{
    /**
     * @var integer
     *
     * @ORM\Column(name="tag_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $tagId;

    /**
     * @var integer
     *
     * @ORM\Column(name="taggable_id", type="integer", nullable=false)
     */
    private $taggableId;

    /**
     * @var string
     *
     * @ORM\Column(name="taggable_type", type="string", length=191, nullable=false)
     */
    private $taggableType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;



    /**
     * Set taggableId
     *
     * @param integer $taggableId
     *
     * @return TaggableTaggables
     */
    public function setTaggableId($taggableId)
    {
        $this->taggableId = $taggableId;

        return $this;
    }

    /**
     * Get taggableId
     *
     * @return integer
     */
    public function getTaggableId()
    {
        return $this->taggableId;
    }

    /**
     * Set taggableType
     *
     * @param string $taggableType
     *
     * @return TaggableTaggables
     */
    public function setTaggableType($taggableType)
    {
        $this->taggableType = $taggableType;

        return $this;
    }

    /**
     * Get taggableType
     *
     * @return string
     */
    public function getTaggableType()
    {
        return $this->taggableType;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return TaggableTaggables
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return TaggableTaggables
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Get tagId
     *
     * @return integer
     */
    public function getTagId()
    {
        return $this->tagId;
    }
}
