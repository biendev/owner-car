<?php

namespace UtilBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TourSlides
 *
 * @ORM\Table(name="tour_slides", indexes={@ORM\Index(name="tour_slides_tour_id_foreign", columns={"tour_id"}), @ORM\Index(name="tour_slides_slide_id_foreign", columns={"slide_id"})})
 * @ORM\Entity
 */
class TourSlides
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \Slides
     *
     * @ORM\ManyToOne(targetEntity="Slides")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="slide_id", referencedColumnName="id")
     * })
     */
    private $slide;

    /**
     * @var \Tours
     *
     * @ORM\ManyToOne(targetEntity="Tours")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tour_id", referencedColumnName="id")
     * })
     */
    private $tour;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return TourSlides
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return TourSlides
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set slide
     *
     * @param \UtilBundle\Entity\Slides $slide
     *
     * @return TourSlides
     */
    public function setSlide(\UtilBundle\Entity\Slides $slide = null)
    {
        $this->slide = $slide;

        return $this;
    }

    /**
     * Get slide
     *
     * @return \UtilBundle\Entity\Slides
     */
    public function getSlide()
    {
        return $this->slide;
    }

    /**
     * Set tour
     *
     * @param \UtilBundle\Entity\Tours $tour
     *
     * @return TourSlides
     */
    public function setTour(\UtilBundle\Entity\Tours $tour = null)
    {
        $this->tour = $tour;

        return $this;
    }

    /**
     * Get tour
     *
     * @return \UtilBundle\Entity\Tours
     */
    public function getTour()
    {
        return $this->tour;
    }
}
