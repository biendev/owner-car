<?php
namespace UtilBundle\Utility;

class Constant {
    const CATEGORY_CODE_TOUR = 2;
    const CATEGORY_CODE_CAR = 1;
    const CATEGORY_CODE_TRANSFER = 3;
    const CATEGORY_LIST = ['Car' => "1", 'Tour' => "2", 'Transfer' => '3'];
}
